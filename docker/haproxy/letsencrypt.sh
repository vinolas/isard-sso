#!/bin/sh
if [ -f /letsencrypt-hook-deploy-concatenante.sh ]
then
  mkdir -p /etc/letsencrypt/renewal-hooks/deploy/
  mv /letsencrypt-hook-deploy-concatenante.sh /etc/letsencrypt/renewal-hooks/deploy/concatenate.sh
fi


if [ -n "$LETSENCRYPT_DOMAIN" -a -n "$LETSENCRYPT_EMAIL" ]
then
  LETSENCRYPT_DOMAIN="$LETSENCRYPT_DOMAIN" crond
  if [ -n "$GANDI_KEY" ]
  then
    touch /gandi.ini
    chmod 600 /gandi.ini
    echo "dns_gandi_api_key=$GANDI_KEY" > /gandi.ini
    GANDI_OPTIONS="--authenticator dns-gandi --dns-gandi-credentials /gandi.ini"
  else
    GANDI_OPTIONS=""
  fi
  if [ ! -f /certs/chain.pem ]
  then
    if certbot certonly $GANDI_OPTIONS -d "$LETSENCRYPT_DOMAIN" -d "*.$LETSENCRYPT_DOMAIN" -m "$LETSENCRYPT_EMAIL" -n --agree-tos
    then
      RENEWED_LINEAGE="/etc/letsencrypt/live/$LETSENCRYPT_DOMAIN" /etc/letsencrypt/renewal-hooks/deploy/concatenate.sh
    fi
  fi
fi
