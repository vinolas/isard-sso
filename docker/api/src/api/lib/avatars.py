#!/usr/bin/env python
# coding=utf-8
import time
from api import app as application
from datetime import datetime, timedelta
import pprint

import logging
import traceback
import yaml, json

from jinja2 import Environment, FileSystemLoader

from keycloak import KeycloakAdmin



class Avatars():
    def __init__(self):
        self.keycloak_admin = KeycloakAdmin(server_url="http://isard-sso-keycloak:8080/auth/",
                                    username='admin',
                                    password='keycloakkeycloak',
                                    realm_name="master",
                                    verify=True)

    def get_user_avatar(self,username):
        return self.keycloak_admin.get_user_id(username)





# # Add user
# new_user = keycloak_admin.create_user({"email": "example@example.com",
#                     "username": "example@example.com",
#                     "enabled": True,
#                     "firstName": "Example",
#                     "lastName": "Example"})
# print(new_user)