from requests import get, post
from admin import app

import logging as log
from pprint import pprint
import os

from minio import Minio
from minio.commonconfig import REPLACE, CopySource
from minio.deleteobjects import DeleteObject

class Avatars():
    def __init__(self):
        self.mclient = Minio(
                            "isard-sso-avatars:9000",
                            access_key="AKIAIOSFODNN7EXAMPLE",
                            secret_key="wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY",
                            secure=False
                        )
        self.bucket='master-avatars'
        self._minio_set_realm()
        # self.update_missing_avatars()

    def add_user_default_avatar(self,userid,role='unknown'):
        self.mclient.fput_object(
                self.bucket, userid, os.path.join(app.root_path,"../custom/avatars/"+role+'.jpg'),
                content_type="image/jpeg ",
        )
        log.warning(' AVATARS: Updated avatar for user '+userid+' with role '+role)

    def delete_user_avatar(self,userid):
        self.minio_delete_object(userid)
        
    def update_missing_avatars(self,users):
        sys_roles=['admin','manager','teacher','student']
        for u in self.get_users_without_image(users):
            try:
                img=[r+'.jpg' for r in sys_roles if r in u['roles']][0]
            except:
                img='unknown.jpg'

            self.mclient.fput_object(
                    self.bucket, u['id'], os.path.join(app.root_path,"../custom/avatars/"+img),
                    content_type="image/jpeg ",
            )
            log.warning(' AVATARS: Updated avatar for user '+u['username']+' with role '+img.split('.')[0])

    def _minio_set_realm(self):
        if not self.mclient.bucket_exists(self.bucket):
            self.mclient.make_bucket(self.bucket)

    def minio_get_objects(self):
        return [o.object_name for o in self.mclient.list_objects(self.bucket)]

    def minio_delete_all_objects(self):
        delete_object_list = map(
            lambda x: DeleteObject(x.object_name),
            self.mclient.list_objects(self.bucket),
        )
        errors=self.mclient.remove_objects(self.bucket, delete_object_list)
        for error in errors:
            log.error(" AVATARS: Error occured when deleting avatar object: "+ error)

    def minio_delete_object(self,oid):
        errors=self.mclient.remove_objects(self.bucket, [DeleteObject(oid)])
        for error in errors:
            log.error(" AVATARS: Error occured when deleting avatar object: "+ error)

    def get_users_without_image(self,users):
        return [u for u in users if u['id'] and u['id'] not in self.minio_get_objects()]