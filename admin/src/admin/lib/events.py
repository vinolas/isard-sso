#!flask/bin/python
# coding=utf-8
from admin import app
import logging as log
import traceback

from uuid import uuid4
import json
from time import sleep
import sys,os
from flask import render_template, Response, request, redirect, url_for, jsonify
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect, send
import base64

class Events():
    def __init__(self,title,text='',total=0,table=False):
        self.eid=str(base64.b64encode(os.urandom(32))[:8])
        self.title=title
        self.text=text
        self.total=total
        self.table=table
        self.item=0
        self.create()

    def create(self):
        log.info('START '+self.eid+': '+self.text)
        app.socketio.emit('notify-create', 
                json.dumps({'id':self.eid,
                            'title':self.title,
                            'text':self.text}), 
                namespace='/sio', 
                room='admin')
        sleep(0.001)

    def __del__(self):
        log.info('END '+self.eid+': '+self.text)
        app.socketio.emit('notify-destroy', 
                json.dumps({'id':self.eid}), 
                namespace='/sio', 
                room='admin')
        sleep(0.001)

    def update_text(self,text):
        self.text=text
        app.socketio.emit('notify-update',
                json.dumps({'id':self.eid,
                            'text':self.text,}), 
                namespace='/sio', 
                room='admin')
        sleep(0.001)

    def append_text(self,text):
        self.text=self.text+'<br>'+text
        app.socketio.emit('notify-update',
                json.dumps({'id':self.eid,
                            'text':self.text,}), 
                namespace='/sio', 
                room='admin')
        sleep(0.001)

    def increment(self,data={'name':'','data':[]}):
        self.item+=1
        log.info('INCREMENT '+self.eid+': '+self.text)
        app.socketio.emit('notify-increment',
                json.dumps({'id':self.eid,
                            'title':self.title,
                            'text': '['+str(self.item)+'/'+str(self.total)+'] '+self.text+' '+data['name'],
                            'item':self.item,
                            'total':self.total,
                            'table':self.table,
                            'data':data}), 
                namespace='/sio', 
                room='admin')
        sleep(0.0001)

    def decrement(self,data={'name':'','data':[]}):
        self.item-=1
        log.info('DECREMENT '+self.eid+': '+self.text)
        app.socketio.emit('notify-decrement',
                json.dumps({'id':self.eid,
                            'title':self.title,
                            'text': '['+str(self.item)+'/'+str(self.total)+'] '+self.text+' '+data['name'],
                            'item':self.item,
                            'total':self.total,
                            'table':self.table,
                            'data':data}), 
                namespace='/sio', 
                room='admin')
        sleep(0.001)

    def reload(self):
        app.socketio.emit('reload',
                json.dumps({}), 
                namespace='/sio', 
                room='admin')
        sleep(0.0001)

    def table(self,event,table,data={}):
        # refresh, add, delete, update
        app.socketio.emit('table_'+event,
                json.dumps({'table':table,'data':data}), 
                namespace='/sio', 
                room='admin')
        sleep(0.0001)