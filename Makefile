#!make
include main.conf
export $(shell sed 's/=.*//' main.conf)

VERSION := 0.0.1-rc0
export VERSION

BUILD_ROOT_PATH=$(shell pwd)

.PHONY: environment
environment:
	cp main.conf .env
	echo "BUILD_ROOT_PATH=$(BUILD_ROOT_PATH)" >> .env

.PHONY: all
all: environment
	cp .env docker-compose-parts
	docker-compose  -f docker-compose-parts/haproxy.yml \
					-f docker-compose-parts/api.yml \
					-f docker-compose-parts/freeipa.yml \
					-f docker-compose-parts/keycloak.yml \
					-f docker-compose-parts/avatars.yml \
					-f docker-compose-parts/postgresql.yml \
					-f docker-compose-parts/network.yml \
					config > docker-compose.yml 

.PHONY: up
up: all
	docker-compose up -d

.PHONY: api
api: environment
	cp .env docker-compose-parts
	docker-compose  -f docker-compose-parts/haproxy.yml \
					-f docker-compose-parts/api.yml \
					-f docker-compose-parts/network.yml \
					config > api.yml 

api-devel: environment
	cp .env docker-compose-parts
	docker-compose  -f docker-compose-parts/haproxy.yml \
					-f docker-compose-parts/api.yml \
					-f docker-compose-parts/api.devel.yml \
					-f docker-compose-parts/network.yml \
					config > api.devel.yml 